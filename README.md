# Timechain Landing Page
Timechain landing page to promote Timechain product.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
### Prerequisites
- NodeJS LTS `https://nodejs.org`
- Angular CLI `https://cli.angular.io/`

### Clone project locally
`git clone git@bitbucket.org:lead-web/timechain-landingpage.git`

### Install NVM (Node Version Manager)
We use NVM to manage our Node verions to keep a consistent environment across developers.

[Follow install instructions here](https://github.com/creationix/nvm#install-script)

After the install, `cd` into the `timechain-landingpage` directory and run `nvm use`. This will use the Node version defined in the `.nvm` file.

### Install project dependencies
Run `npm install` into the `timechain-landingpage` directory to install all the nodes packages.

## Development

### Start webpack development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Deployment

### Build the project
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### AWS
The project is hosted on AWS in a S3 bucket. To update the production version, upload the content of the `dist/` directory into `www.timechain.com` bucket and make sure to select `Grant public read access to this object(s)` in the `Manage public permissions` select field.

## Build with
* [Angular CLI](https://cli.angular.io/) - The web framework used
* [NPM](https://www.npmjs.com/) - Dependency Management
* [SASS](https://sass-lang.com/guide) - CSS Preprocessor scripting language

## Working/Branching
- Always branch from master to start working on a feature or a bug fix
- Once a feature is ready to go live, merge your branch on master

## Miscellaneous
- Local dev environment: `http://localhost:4200/`
- Production environment: `https://www.timechain.com/`
