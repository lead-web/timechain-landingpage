import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  public teamMembers: any[];
  public advisors: any[];
  public logos: any[];
  public infos: any[];

  constructor() {

  }

  ngOnInit() {

    this.teamMembers = [
      {
        'name': 'Louis Cleroux',
        'photo': 'louis-cleroux',
        'title': 'Chief Executive Officer',
        'linkedin': 'https://www.linkedin.com/in/lcleroux'
      },
      {
        'name': 'Phil Therien',
        'photo': 'phil-therien',
        'title': 'Chief Operating Officer',
        'linkedin': 'https://www.linkedin.com/in/philtherien'
      },
      {
        'name': 'Marc-Andre Lacombe',
        'photo': 'marc-lacombe',
        'title': 'Chief Technology Officer',
        'linkedin': 'https://www.linkedin.com/in/marcandrelacombe'
      },
      {
        'name': 'Andre Dupont',
        'photo': 'andre-dupont',
        'title': 'VP Strategy',
        'linkedin': 'https://www.linkedin.com/in/andredupontmoguel'
      },
      {
        'name': 'Eamon Leonard',
        'photo': 'eamon-leonard',
        'title': 'VP Marketing',
        'linkedin': 'https://www.linkedin.com/in/leonardeamon'
      },
      {
        'name': 'Kosta Kostic',
        'photo': 'kosta-kostic',
        'title': 'Legal Counsel',
        'linkedin': 'https://www.linkedin.com/in/kosta-kostic-273804'
      }
    ];

    this.advisors = [
      {
        'name': 'Louis Doyle',
        'title': 'Market Advisor',
        'linkedin': 'https://www.linkedin.com/in/louis-doyle-2989b31'
      },
      {
        'name': 'Louis Turp',
        'title': 'Venture Advisor',
        'linkedin': 'https://www.linkedin.com/in/louis-turp-5899611'
      },
      {
        'name': 'Jack Luo',
        'title': 'Blockchain Advisor',
        'linkedin': 'https://www.linkedin.com/in/jackwluo'
      },
      {
        'name': 'Alexandre Cote',
        'title': 'Financial Markets Advisor',
        'linkedin': 'https://www.linkedin.com/in/alexandre-c%C3%B4t%C3%A9-0102951'
      },
      {
        'name': 'Charles Cao',
        'photo': 'eamon-leonard',
        'title': 'AI & Blockchain Advisor',
        'linkedin': 'https://www.linkedin.com/in/charles-cao-09a79526'
      }
    ];

    this.logos = [
      {
        'path': 'bitcoin-logo',
        'name': 'bitcoin'
      },
      {
        'path': 'eos-logo',
        'name': 'eos'
      },
      {
        'path': 'ethereum-logo',
        'name': 'ethereum'
      },
      {
        'path': 'litecoin-logo',
        'name': 'litecoin'
      },
      {
        'path': 'ripple-logo',
        'name': 'ripple'
      },
      {
        'path': 'stellar-logo',
        'name': 'stellar'
      },
      {
        'path': 'bitcoin-cash-logo',
        'name': 'bitcoin-cash'
      },
      {
        'path': 'nem-logo',
        'name': 'nem'
      },
      {
        'path': 'cardano-logo',
        'name': 'cardano'
      },
      {
        'path': 'neo-logo',
        'name': 'neo'
      }
    ]

    this.infos = [
      {
        'title': 'Timechain MetaWallet is the only wallet you’ll ever need',
        'text': 'The Timechain MetaWallet will store more than 1000 types of cryptocurrencies, allowing users to create multiple same chain wallets and categories to simplify asset management.',
        'img': 'payment'
      },
      {
        'title': 'Decentralized and centralized exchanges integrated with MetaWallet',
        'text': ' Decentralized exchange supports many coins and eliminates custodianship risk. Centralized exchange for top 20 cryptos with built-in cold storage.',
        'img': 'arrows'
      },
      {
        'title': 'Allow purchases and withdrawals directly in-wallet',
        'text': 'Timechain will eliminate the need to go to external, online exchanges by allowing users to purchase or sell crypto directly from their wallet, making the process much safer and simpler.',
        'img': 'database'
      },
      {
        'title': 'Secure, trusted digital asset management',
        'text': 'Timechain has been developed according to the highest standards of security in the industry today. Security upgrades and penetration testing are ongoing.',
        'img': 'lock'
      },
    ]

  }

}

