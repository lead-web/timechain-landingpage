import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

import { GoogleAnalyticsModule, GA_TOKEN } from 'angular-ga';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    GoogleAnalyticsModule.forRoot()
  ],
  providers: [{ provide: GA_TOKEN, useValue: 'UA-118601095-1' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
