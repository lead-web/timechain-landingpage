import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { DialogComponent } from './dialog/dialog.component';
import { MatInputModule, MatFormFieldModule, MatButtonModule, MatDialogModule, MatFormFieldControl, MatDialogClose } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule
  ],
  declarations: [
    DialogComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent
  ],
  exports: [
    DialogComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent
  ],
  entryComponents: [
    DialogComponent
  ]
})
export class CoreModule { }
