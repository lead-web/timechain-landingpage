import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';

import { faTelegramPlane } from '@fortawesome/fontawesome-free-brands';
import fontawesome from '@fortawesome/fontawesome';

fontawesome.library.add(faTelegramPlane);

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})

export class NavComponent implements OnInit {

  @ViewChild('navbarResponsive') navbar: ElementRef;
  public isOpen: boolean;
  public animal: string;
  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.isOpen = false;
  }

  openDialog(width:string, height:string): void {
    console.log("whitepaper")

    let dialogRef = this.dialog.open(DialogComponent, {
      width: width,
      height: height,
      data: { animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  openNav(): void {
    console.log("navbarResponsive");
    if (this.isOpen) {
      this.isOpen = false;
    } else {
      this.isOpen = true;
    }

  }
}